/*
	Nome: Marllon Lucas Rodrigues Rosa

	O sistema tem é dividio em 3 pastas

	Root -> Contém o arquivo principal do simulador
	Root/lib -> Pasta onde contem algumas funções que foi implementada diretamente para este trabalho
	Root/lib/include -> Pasta que contem as header das classes utilzada por esse programa

	Como compilar:
		
		Basta apenas dar "make" no terminal que o proprio sistema irá gerar um arquivo "simulador"

	Como executar:

		Na linha de comando basta fazer:

			./simulador < input.txt

		onde input.txt é um arquivo de uma unica linha contento todos os paramentros para o meu programa no formato:
		tmp, n, tb (Lista de processos)

		Caso exista um arquivo chamado "input.txt" dentro da minha pasta root, basta executar o comando:

			sh main.sh

		Que o sistema vai compilar o programa e ainda executar a saida com os dados que estao no arquivo input.txt

	IMPORTANTE: O programa foi desenvolvido para um numero de  processos maiores que 0, portanto para n = 0, o programa não funciona

*/

#include <iostream>
#include <thread>
#include <string>
#include <stdlib.h>
#include <mutex>
#include <condition_variable>
#include <time.h>

using namespace std;

#include "lib/include/List.h"

#define FALSE 0
#define TRUE 1

// definição das variveis de comunicação entre threads
bool FINISH_PROCESS  = FALSE;
bool FINISH = FALSE;
bool FCFS_TO_SWAPEER = FALSE;
bool RESET_TIMER = FALSE;
bool TIMER_TO_ROBIN = FALSE;
bool ROBIN_TO_DISPATCHER = FALSE;
bool DISPATCHER_TO_ROBIN = FALSE;
bool DISPATCHER_TO_SWAPPER = FALSE;
bool SWAPPER_TO_FCFS = FALSE;
bool SWAPPER_TO_DISPATCHER = FALSE;

bool FINISH_ALL = FALSE;

// Definição das variaveis de controle da sincronização das threads
bool SYNC_RESET = FALSE;
bool SYNC_PROCESS = FALSE;
bool SYNC_FINISH = FALSE;
bool SYNC_FCFS = FALSE;
bool SYNC_ROBIN = FALSE;
bool SYNC_DISPATCHER = FALSE;
bool SYNC_SWAPPER = FALSE;

typedef struct memory{
	int bytes;
	int freeBytes;
	List Queue;
} _RAM;

typedef struct disk{
	List Queue;
} _DISK;

typedef struct timer{
	int TimeQuantum;
	int TimeExecute;
	int CountTime;
} _TIMER;

typedef struct CPU{
	int id;
	int BurstLeft;
	int Active;
} _CPU;


std::string msg;

// Variaveis globais
// Memoria RAM
_RAM * RAM;
// Disco Rigido
_DISK * DISK;
// Temporizador do sistema
_TIMER * TIMER;
// Variavel que armazena o processo que esta ultilizando a CPU
_CPU * CPU;

List Input = List();

// Listas ultilizadas pelo gerenciamento e escalonamento da CPU
List WaitQueue = List();
List ReadyQueue = List();

// Inicialização dos mutexes usados pelo programa
std::mutex message;
std::mutex _mutex;
std::mutex syncM;

// Variavel que indica que o timer foi atualizado
std::condition_variable _timer_var;

// Funções auxiliares
void StartingValues(int tmp, int tq);
void printMensage(string Massage);
void copyCPU(_register * node);
void attTimer();
bool is_finish();
void syncReset();
bool sync();
void freeAll();

// Threads
void Dispatcher();
void schedulerFCFS();
void ProcessCreator();
void Swapper();
void schedulerRoundRobin();
void resetTimer();
void Timer();


int main(int argc, char const *argv[]){

	int tmp, n, tq;

	cin >> tmp;
	cin >> n; 
	cin >> tq; 
	
	int id, tp, tc, tb;

	// Init variables
	StartingValues(tmp, tq);

	// Get the data of the input
	for (int i = 0; i < n; i++){
		_register * REG = (_register *) malloc(sizeof(_register));
		
		cin >> id;
		cin >> tp;
		cin >> tc;
		cin >> tb;

		REG->id = id;
		REG->tp = tp;
		REG->tc = tc;
		REG->tb = tb;
		REG->BurstLeft = REG->tb;

		// Insere na minha lista de entrada
		Input.insertFirst(REG);
	}

	// Ordena a minha lista com base no tempo em que o processo entra
	Input.sort();

	msg = "Inicio da observação";
	printMensage(msg);

	std::thread t1(Timer);
	std::thread t2(resetTimer);
	std::thread t3(ProcessCreator);
	std::thread t4(schedulerFCFS);
	std::thread t5(schedulerRoundRobin);
	std::thread t6(Dispatcher);
	std::thread t7(Swapper);

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();

	msg = "Término da observação";
	printMensage(msg);

	freeAll();

	return 0;
}

// Libera a memoria alocada
void freeAll(){
	RAM->Queue.clear();
	DISK->Queue.clear();

	Input.clear();
	WaitQueue.clear();
	ReadyQueue.clear();

	free(TIMER);
	free(RAM);
	free(DISK);
	free(CPU);
}

void StartingValues(int tmp, int tq){
	/*Inicialização das estruturas globais utlizada pelo programa*/
	RAM 	= (_RAM*) malloc(sizeof(_RAM));
	DISK 	= (_DISK*) malloc(sizeof(_DISK));
	TIMER 	= (_TIMER*) malloc(sizeof(_TIMER));
	CPU 	= (_CPU *) malloc(sizeof(_CPU));

	DISK->Queue = List();

	// Inicializa a memoria RAM
	RAM->bytes = tmp;
	RAM->freeBytes = tmp;
	RAM->Queue = List();

	// Inicializa os valores do timer
	TIMER->TimeQuantum = tq;
	TIMER->TimeExecute = -1;
	TIMER->CountTime = 0;

	// Inicializa um valor inicial para a CPU "executar"
	CPU->id = -1;
	CPU->BurstLeft = 0;
	CPU->Active = 0;
}

std::mutex s;

void copyCPU(_register * node){
	s.lock();
		CPU->id = node->id;
		CPU->BurstLeft = node->BurstLeft;
		CPU->Active = FALSE;
	s.unlock();
}

void attTimer(){
	s.lock();
		TIMER->TimeExecute += 1;
		TIMER->CountTime += 1;
	s.unlock();
}

void printMensage(string Massage){
	message.lock();
		time_t rawtime;
		struct tm * timeinfo;

		time ( &rawtime );
		timeinfo = localtime (&rawtime );

		Massage = std::to_string(timeinfo->tm_hour) + ":" + std::to_string(timeinfo->tm_min) + ":" + std::to_string(timeinfo->tm_sec) + " - " + Massage;

		std::cout << Massage << std::endl;
	message.unlock();
}

void printList(t_node * list){

	message.lock();

	t_node * pointer = list;

	while(pointer != NULL){
		cout << "Processo: "  << pointer->Registro->id << endl;
		cout << "Memoria: "   << pointer->Registro->tp << endl;
		cout << "Entrada: "   << pointer->Registro->tc << endl;
		cout << "Tempo CPU: " << pointer->Registro->tb << endl;
		cout << "Tempo Restante: " << pointer->Registro->BurstLeft << endl;
		cout << endl;
		pointer = pointer->next;
	}

	message.unlock();

}

// Função que mostra se o programa foi sincronizado
bool sync(){
	return SYNC_RESET && SYNC_PROCESS && SYNC_FCFS && SYNC_SWAPPER && SYNC_ROBIN && SYNC_DISPATCHER;
}

// Função que reseta as flags de sincronismos do sistema
void syncReset(){
	syncM.lock();
		SYNC_RESET = FALSE;
		SYNC_PROCESS = FALSE;
		SYNC_FCFS = FALSE;
		SYNC_ROBIN = FALSE;
		SYNC_DISPATCHER = FALSE;
		SYNC_SWAPPER = FALSE;
	syncM.unlock();
}


// Threads que serao executadas

std::mutex _timer;
std::mutex _process;
std::mutex _FCFS;
std::mutex _swapper;

std::condition_variable _finish_process;
std::condition_variable _robin;
std::condition_variable _cond_reset;


bool is_finish(){
	int size = Input.size() + WaitQueue.size() + ReadyQueue.size();
	return !size;
}

void Timer(){
	while(!is_finish()){
		// Caso todos os outros processos execultaram eu atualizo o meu timer
		if (sync()){
			_mutex.lock();
				syncReset();

				_timer.lock();
					// Adiciona a cada iteracao do meu programa 1 segundo a mais
					attTimer();

					// Atualizo o tempo restante do burst do processo
					if(CPU->Active == TRUE){
						CPU->BurstLeft = CPU->BurstLeft - 1;
					}
				_timer.unlock();

				// Aviso as minhas threads que eu atualizei o timer
				_timer_var.notify_all();

				
					// Reseta o timer quando o mesmo bater o valor de Time Quantum
					if((TIMER->TimeExecute >= 0) && (TIMER->CountTime == TIMER->TimeQuantum || CPU->BurstLeft <= 0)){
						if(CPU->BurstLeft <= 0 && CPU->Active == TRUE){
							FINISH_PROCESS = TRUE;
						} 
						// Aviso ao escalonador que ele pode trocar de processo
						TIMER_TO_ROBIN = TRUE;
					}
				
			_mutex.unlock();
			// Adiciona um dalay na minha thread
			std::this_thread::sleep_for(chrono::milliseconds(700));
		}
	}
	_timer_var.notify_all();
}


void ProcessCreator(){

	while(!is_finish()){
	
		if(!sync()){

			SYNC_PROCESS = TRUE;

			std::unique_lock<mutex> locker(_mutex);
			// Aguarda o timer sincronizar com as threads
			_timer_var.wait(locker);
			
			if(Input.size() > 0){

				int id = Input.first()->id;
				int tc = Input.first()->tc;

				_process.lock();
					if (TIMER->TimeExecute == tc){
						// Adiciono o meu elemento na minha lista de espera
						WaitQueue.insertLast(Input.first());

						msg = "Criador de processos criou o processo " + to_string(Input.first()->id) + " e o colocou na fila de entrada";
						printMensage(msg);				
						
						// Removo o elemento do meu disco
						Input.remove(id);
					}
				_process.unlock();
			}

			locker.unlock();
		}
	}
}

void schedulerFCFS(){
	while (!is_finish()){


		if(!sync()){
			SYNC_FCFS = TRUE;
			
			std::unique_lock<mutex> locker(_mutex);
			// Aguarda o timer sincronizar com as threads
			_timer_var.wait(locker);

			int sizeRam = RAM->freeBytes;

			if (WaitQueue.size() > 0){

				// Memoria restante na RAM
				int sizeLeft = sizeRam - WaitQueue.first()->tp;

				// Caso o processo indicado tenha memoria suficiente, então eu adiciono o processo e atualizo a memoria RAM
				// Verifica se há espaço na memoria ou se algum programa terminou o seu burst, alem disso o swapper nao dever
				// mandar uma resposta
				if(((sizeLeft >= 0) || FINISH_PROCESS) && !FCFS_TO_SWAPEER){
					FCFS_TO_SWAPEER = TRUE;
				}

				// Caso o swapper mande uma confirmacao para o escalonador
				if(SWAPPER_TO_FCFS){

					// Atualizo a minha frag para a proxima chamada do swapper
					SWAPPER_TO_FCFS = FALSE;
					
					_FCFS.lock();
						msg = "Swapper avisa o Escalonador FCFS de longo prazo que o processo " + std::to_string(WaitQueue.first()->id) + " está na memória";
						printMensage(msg);
						
						msg =  "Escalonador FCFS de longo prazo escolheu e retirou o processo "+ std::to_string(WaitQueue.first()->id) +" da fila de entrada";
						printMensage(msg);

						// Insere o processo Pj na fila de prontos ja que o mesmo se encontra na memoria
						ReadyQueue.insertLast(WaitQueue.first());

						msg = "Escalonador FCFS de longo prazo colocou "+ std::to_string(WaitQueue.first()->id) +" na fila de prontos";
						printMensage(msg);

						int id = WaitQueue.first()->id;

						// Remove o mesmo na fila de entrada
						WaitQueue.remove(id);
					_FCFS.unlock();
				}
			}
			locker.unlock();		
		}
	}

}

void Swapper(){
	while(!is_finish()){
		if(!sync()){
			SYNC_SWAPPER = TRUE;

			std::unique_lock<mutex> locker(_mutex);
			// Aguarda o timer sincronizar com as threads
			_timer_var.wait(locker);
			
			int sizeRam = RAM->freeBytes;

			// Caso ele tenha recebido uma notificacao do escalonador FCFS
			if(FCFS_TO_SWAPEER){
				if(WaitQueue.size() > 0){

					msg = "Escalonador FCFS de longo prazo solicitou que o Swapper traga " +  std::to_string(WaitQueue.first()->id) + " à memória";
					printMensage(msg);

					int sizeLeft = sizeRam - WaitQueue.first()->tp;

					// Caso tenha memoria suficiente
					if(sizeLeft >= 0){
						msg = "Swapper percebe que há espaço no processo " +  std::to_string(WaitQueue.first()->id) + " na memória";
						printMensage(msg);
					
					// Caso eu nao encontre espaço na minha memeria ram
					}else{ 
						msg = "Swapper percebe que não há espaço no processo " +  std::to_string(WaitQueue.first()->id) + " na memória";
						printMensage(msg);

						do{
							_swapper.lock();
								// Adiciono o primeiro elemento da minha ram ao meu disco
								DISK->Queue.insertLast(RAM->Queue.first());
								// Atualizo o espaço disponivel da minha ram
								RAM->freeBytes = RAM->freeBytes + RAM->Queue.first()->tp;
								
								int id = RAM->Queue.first()->id;

								msg = "Swapper retirou o processo " +  std::to_string(id) + " para liberar espaço na memória, e o enviou ao disco";
								printMensage(msg);

								// Removo o elemento da minha ram
								RAM->Queue.remove(id);

								// Revejo o espaço da minha ram
								sizeLeft = RAM->freeBytes - WaitQueue.first()->tp;
							_swapper.unlock(); 

						}while(sizeLeft < 0);
					}

					_swapper.lock();
						// Coloca meu processo na memoria RAM
						RAM->Queue.insertFirst(WaitQueue.first());
						// Atualizo o espaço disponivel da minha memoria ram
						RAM->freeBytes = sizeLeft;
						// Aviso o meu escalonador que esta tudo pronto
						SWAPPER_TO_FCFS = TRUE;

						FCFS_TO_SWAPEER = FALSE;
					_swapper.unlock();
				}
			}

			if (DISPATCHER_TO_SWAPPER){

				int sizeLeft = sizeRam - ReadyQueue.first()->tp;

				if(sizeLeft > -1){
					msg = "Swapper percebe que há espaço no processo " +  std::to_string(ReadyQueue.first()->id) + " na memória" ;
					printMensage(msg);
				
				// Caso eu nao encontre espaço na minha memeria ram
				}else{
					msg = "Swapper percebe que não há espaço no processo " +  std::to_string(ReadyQueue.first()->id) + " na memória" ;
					printMensage(msg);

					do{
						// Adiciono o primeiro elemento da minha ram ao meu disco
						DISK->Queue.insertLast(RAM->Queue.first());
						// Atualizo o espaço disponivel da minha ram
						RAM->freeBytes = RAM->freeBytes + RAM->Queue.first()->tp;
						
						msg = "Swapper retirou o processo " +  std::to_string(RAM->Queue.first()->id) + " para liberar espaço na memória, e o enviou ao disco" ;
						printMensage(msg);

						// Removo o elemento da minha ram
						RAM->Queue.remove(RAM->Queue.first()->id);

						// Revejo o espaço da minha ram
						sizeLeft = RAM->freeBytes - ReadyQueue.first()->tp; 

					}while(sizeLeft < 1);
				}

				// Coloca meu processo na memoria RAM
				RAM->Queue.insertFirst(ReadyQueue.first());
				// Atualizo o espaço disponivel da minha memoria ram
				RAM->freeBytes = sizeLeft;
				// Aviso o meu escalonador que esta tudo pronto
				SWAPPER_TO_DISPATCHER = TRUE;

				msg = "Swapper avisa o Despachante que o processo "+  std::to_string(ReadyQueue.first()->id) +" está na memória";
				printMensage(msg);

				DISPATCHER_TO_SWAPPER = FALSE;			
			}

			locker.unlock();
		}
	}
}

std::mutex _mutex_robin_timer;
std::mutex _mutex_finish_process;

void schedulerRoundRobin(){

	while(!is_finish()){

		if(!sync()){

			SYNC_ROBIN = TRUE;
			// Termino do programa

			std::unique_lock<mutex> locker(_mutex);
			// Aguarda o timer sincronizar com as threads
			_timer_var.wait(locker);

			if(TIMER_TO_ROBIN){
		
				TIMER_TO_ROBIN = FALSE;

				if(CPU != NULL){
					// Desativa a minha cpu
					CPU->Active = FALSE;


					if(CPU->id >= 0 && CPU->Active == TRUE){
						msg = "Timer informa ao Escalonador Round-Robin de CPU que o processo "+ std::to_string(CPU->id) +" atualmente em execução precisa ser retirado da CPU" ;
						printMensage(msg);
					}

					if (FINISH_PROCESS){
						FINISH_PROCESS = FALSE;

						if(CPU->id >= 0){
							msg = "Processo "+  std::to_string(CPU->id) +" terminou sua execução" ;
							printMensage(msg);
							// Remove o processo da fila
							ReadyQueue.remove(CPU->id);

							CPU->Active = FALSE;
						}

					}

					if(ReadyQueue.first()){
						msg = "Escalonador Round-Robin de CPU escolheu o processo " + std::to_string(ReadyQueue.first()->id) + ", retirou-o da fila de prontos e o encaminhou ao Despachante" ;
						printMensage(msg);
					}
				}
				
				// Avisa ao despachante que é para trocar o processo corrente
				ROBIN_TO_DISPATCHER = TRUE;
				RESET_TIMER = TRUE;
			}

			locker.unlock();
		}
	}
}


std::mutex queue;

void Dispatcher(){
	while(!is_finish()){
		
		if(!sync()){

			SYNC_DISPATCHER = TRUE;
			
			std::unique_lock<mutex> locker(_mutex);			
			// Aguarda o timer sincronizar com as threads
			_timer_var.wait(locker);

			if(ROBIN_TO_DISPATCHER){
				ROBIN_TO_DISPATCHER = FALSE;

				if (ReadyQueue.size() > 0){

					// Verifica se o proximo processo esta na memoria RAM
					if(RAM->Queue.search(ReadyQueue.first()->id) != NULL){

						msg = "Despachante percebe que o processo "+  std::to_string(ReadyQueue.first()->id) +" está na memória" ;
						printMensage(msg);

						queue.lock();

							_register * r = ReadyQueue.search(CPU->id);

							if(r){
								r->BurstLeft = CPU->BurstLeft;
							}

							// Troca o processo na CPU
							copyCPU(ReadyQueue.first());
							// Ativa a CPU
							CPU->Active = TRUE;

							// Atualizo a minha lista round robin;
							ReadyQueue.changeForLast();

						queue.unlock();

						msg = "Despachante reiniciou o Timer com "+ std::to_string(TIMER->TimeQuantum) +" e liberou a CPU ao processo " +  std::to_string(CPU->id) ;
						printMensage(msg);

						
					}else{

						DISPATCHER_TO_SWAPPER = TRUE;

						msg = "Despachante percebe que o processo " +  std::to_string(ReadyQueue.first()->id) + " está no disco e solicita que o Swapper traga " +  std::to_string(ReadyQueue.first()->id) + " à memória" ;
						printMensage(msg);

						if(SWAPPER_TO_DISPATCHER){

							msg = "Despachante é avisado pelo Swapper que o processo "+  std::to_string(ReadyQueue.first()->id) +" está na memória" ;
							printMensage(msg);

							queue.lock();

								_register * r = ReadyQueue.search(CPU->id);

								if(r){
									r->BurstLeft = CPU->BurstLeft;
								}

								// Libera a CPU para o processo corrente
								copyCPU(ReadyQueue.first());
								// Ativa a CPU
								CPU->Active = TRUE;

								// Atualizo a minha lista round robin;
								ReadyQueue.changeForLast();

							queue.unlock();

							msg = "Despachante reiniciou o Timer com "+ std::to_string(TIMER->TimeQuantum) +" e liberou a CPU ao processo " +  std::to_string(CPU->id);
							printMensage(msg);

							SWAPPER_TO_DISPATCHER = FALSE;
						}
					}
				}
			}

			locker.unlock();
		}
	}
}

std::mutex _reset;

// Reseta o timer quando o mesmo receber uma notificacao
void resetTimer(){
	while (!is_finish()){

		if(!sync()){
			SYNC_RESET = TRUE;

			std::unique_lock<mutex> locker(_mutex);
			// Aguarda o timer sincronizar com as threads
			_timer_var.wait(locker);
	
				_reset.lock();
					if(RESET_TIMER){	
						RESET_TIMER = FALSE;
						// Apos receber a mensagem apenas troca a flag novamente 
						TIMER->CountTime = 0;
					}
				_reset.unlock();

			locker.unlock();
		}
	}
}