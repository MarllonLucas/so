#ifndef __LIST__
#define __LIST__

typedef struct registro{
	// Identificador do processo valor dade de 0 < id < n - 1
	int id;
	// Tamanho do processo que sera alocado na memoria
	int tp;
	// Tempo de chegada na fila
	int tc;
	// Tempo em que o processo sera executado na CPU
	int tb;	
	// Tempo restante para o termino do bust de CPU
	int BurstLeft;
	// Atributo que indica que este processo esta sendo utilizado pela CPU
	int Active;
} _register;

// Estrutura que armazena o no da minha lista
typedef struct node{
	_register * Registro;
	struct node * next;
	struct node * previous;
} t_node;

// Estrutura que armazena a header da minha lista
typedef struct list{
	t_node * first;
	t_node * last;
	int counter;
} _list;


// Classe que gerencia a minha classe da lista
class List{
	

	protected:
		_list * Lista;
		
		t_node * allocate(_register * REG);
		t_node * searchNode(int Key);
		int comp(int Key, t_node * node);
		void clearList();

	public:
		// Construtor da classe
		List();
		
		// Funcoes publicas para os usuarios 
		void insertFirst(_register * REG);
		void insertLast(_register * REG);
		bool remove(int Key);
		_register * search(int Key);
		_register * first();
		t_node * getList();
		void changeForLast();

		void sort();
		int size();
		void clear();
	
};

#endif