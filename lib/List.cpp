#include "include/List.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <algorithm>

// ###########################################################
// ##################### PUBLIC FUNCTIONS ####################
// ###########################################################

// Inicializa a minha lista
List::List(){
	Lista = (_list *) malloc(sizeof(_list));
	Lista->counter = 0;
	Lista->first = NULL;
	Lista->last = NULL; 
}

void List::insertFirst(_register * REG){

	// Create the node
	t_node * element = allocate(REG);

	// If the first is NULL, then I'm unique
	if (Lista->first == NULL) {
		// I'm the first
		Lista->first = element;
		// Point to myself
		element->next = NULL;
		element->previous = NULL;
		// I'm the last
		Lista->last = element;
	} else {
		// Make me point to the original first
		element->next = Lista->first;
		// Keep the bound
		element->previous = NULL;
		// Make the sync
		Lista->first->previous = element;
		// Change the first
		Lista->first = element;
	}

	// Increment the counter
	Lista->counter++;

}

void List::insertLast(_register * REG){
	// Create the node
	t_node * element = allocate(REG);

	// If the last is NULL, then I'm unique
	if (Lista->last == NULL) {
		// I'm the first
		Lista->first = element;
		// Point to myself
		element->next = NULL;
		element->previous = NULL;
		// I'm the last
		Lista->last = element;
	} else {
		// Keep not the circular bound
		element->next = NULL;
		// Make the sync
		Lista->last->next = element;
		// Keep the bound
		element->previous = Lista->last;
		// Lista->first->previous = element;
		// I'm the last
		Lista->last = element;
	}

	// Increment the counter
	Lista->counter++;
}

bool List::remove(int Key) {
    t_node * LastButOne = NULL;
    t_node * second = NULL;
    bool ret = 0;

    // Faz a busca pelo meu registro
    LastButOne = searchNode(Key);

    // Ajusta os ponteiros da minha lista
    if (LastButOne != NULL){
    	if(size() > 0){
    		Lista->counter--;

    		// Caso tenha apenas 1 elemento em minha lista
    		if(size() == 0){
    			Lista->first = NULL;
    			Lista->last = NULL;
    		}else{

    			if(LastButOne == Lista->first){
    				
    				second = LastButOne->next;
    				second->previous = NULL;

    				Lista->first = second;

    			}else if(LastButOne == Lista->last){

    				second = LastButOne->previous;
    				second->next = NULL;

    				Lista->last = second;

    			}else{

    				second = LastButOne;
    				second->previous->next = second->next;
    				second->next->previous = second->previous;
    			
    			}
    		}
			// Apago o no da memoria
			free(LastButOne);
			ret = 1;
    	}
    }

    return ret;
}

_register * List::search(int Key){
    t_node * pointer;
    _register * result = NULL;

    pointer = searchNode(Key);

	if(pointer != NULL)
		result = pointer->Registro;

    // Retorna o resultado da minha busca
    return result;
}

// Retorna o tamanho da minha lista
int List::size(){
	return Lista->counter;
}

// Retorna o primeiro elemento da minha fila
_register * List::first(){
	_register * ret = NULL;

	if(Lista->first)
		ret = Lista->first->Registro;

	return ret;
}

_register * copyRegister(_register * REG){
	_register * FReg = (_register *) malloc(sizeof(_register));

	FReg->id = REG->id;
	FReg->tp = REG->tp;
	FReg->tc = REG->tc;
	FReg->tb = REG->tb;

	return FReg;
}

void List::changeForLast(){

	_register * FReg = copyRegister(Lista->first->Registro);

	remove(Lista->first->Registro->id);

	insertLast(FReg);

}

void List::clearList(){
	t_node * pointer = Lista->first;
	t_node * trash = NULL;

	while(pointer != NULL){
		trash = pointer;
		pointer = pointer->next;

		free(trash->Registro);
		free(trash);
	}
}

void List::clear(){

	clearList();

	free(Lista);
}

t_node * List::getList(){
	return Lista->first;
}

bool compSort(t_node * a, t_node * b){
	return (a->Registro->tc < b->Registro->tc);
}

void List::sort(){
	std::vector<t_node *> list;

	t_node * pointer = Lista->first;

	while(pointer != NULL){
		list.push_back(pointer);
		pointer = pointer->next;
	}

	std::stable_sort (list.begin(), list.end(), compSort);

	Lista->first = list[0];

	if(list.size() > 1)
		Lista->first->next = list[1];

	for (unsigned int i = 1; i < list.size(); ++i){
		
		// Atualizacao dos ponteiros
		if(list[i+1]) 	list[i]->next = list[i+1];
		else 			list[i]->next = NULL;

		list[i]->previous = list[i-1];
	}

}

// ###########################################################
// #################### PRIVATE FUNCTIONS ####################
// ###########################################################

int List::comp(int Key, t_node * node){
	return (Key == node->Registro->id);
}

t_node * List::allocate(_register * REG){
	t_node * node = (t_node *) malloc(sizeof(t_node));
	node->Registro = REG;

	return node;
} 

t_node * List::searchNode(int Key){
    t_node * pointer = Lista->first;

	while(pointer != NULL && !comp(Key, pointer))
		pointer = pointer->next;

    // Retorna o resultado da minha busca
    return pointer;
}