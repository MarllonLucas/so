Programa destinado a emulação de um escalonamento de processos no Sistema Operacional


Estruturação de pastas:

LIB: 
	Pasta destinada as classes criadas para a execução do programa

LIB -> INCLUDE:
	Pasta destinada as header do programa

Compilação:
	
	Apenas basta dar comando "make" no terminal que a compilação será automaticamente realizada
	um arquivo com o nome de main será criado na pasta raiz, basta então chama-lo pelo terminal 
	e passar os parametros na qual ele precisa.