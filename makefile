all: Sys clean

Sys: simulator.o list.o
	g++ -std=c++11 -g -o simulator simulator.o list.o -pthread

simulator.o: simulator.cpp
	g++ -std=c++11 -Wall -pedantic -c -g -o simulator.o simulator.cpp -pthread

list.o: lib/List.cpp lib/include/List.h
	g++ -std=c++11 -Wall -pedantic -c -g -o list.o lib/List.cpp

clean:
	rm *.o
